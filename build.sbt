name := "Scala/SBT project"

version := "0.1.0"

scalaVersion := "2.11.11"

scalacOptions ++= Seq(
  "-target:jvm-1.8",
  "-encoding", "UTF-8",
  "-unchecked",
  "-deprecation",
  "-Xfuture",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard"
)

libraryDependencies ++= {
  Seq("junit" % "junit" % "4.12")
}

libraryDependencies ++= Seq(
  "com.cra.figaro" %% "figaro" % "4.1.0.0",
  "org.scalatest" %% "scalatest" % "3.0.1",
  "junit" % "junit" % "4.12"
)
