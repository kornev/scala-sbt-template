package com.empty

import scala.beans.BeanProperty

object Main extends App {
  val language = "Scala"
  println(s"I'm empty $language project.")
}

// O.type, Object extends Trait -> Singleton
// infex notation, infex types
// Case Class as pattern
// Unit == ()

class Person0 protected (@BeanProperty var name: String, no: Int = 1) {
  private[this] var _age: Int = _
  private val _no = this.no

  def this() = this(name = Person0.DEFAULT_NAME)

  def age: Int = _age
  def age_=(value: => Int): Unit = _age = value
  def unary_~(): Person0 = new Person0(s"№ $no", no + 1)
  def update(age: Int): Unit = _age = age  // (new Person0)() = 9
}

object Person0 {
  var a, b, c = new Person0  // new, new, new
  // val x = (a = null)  // x == Unit
  private val DEFAULT_NAME = "Mike"

  def show(p: Person0): Unit = {
    import p._
    val inc: Int => Int = 1 + _  // val inc = (x: Int) => x + 1
                                 // val inc = { x: Int => x + 1 }
                                 // val inc: Int => Int = x => x + 1
    for (i <- 1 to 2)
      println(inc(i + _no) + s") Name: $name")
  }
  def apply(name: String, no: Int) = new Person0(name, no)
  // def unapply(p: Person0): Option[(String, Int)] = Some(p.name, p.age)
}

object Lib {
  type BigIntSet = BigInt => Boolean
  implicit class TmpWrapper[A](p0: A => Boolean) {
    def ?(elem: A): Boolean = p0(elem)
    def |(p1: A => Boolean): A => Boolean =
      x => p0(x) | p1(x)
  }
}

object Demo0 {
  import Lib._

  val set0: BigIntSet = _ > 10
  val set1: BigIntSet = _ < 20
  val status = (set0 | set1) ? 11
  val set3: Int => Boolean = Set(1, 2, 3)
  val set4: Iterable[Int] = Set(1, 2, 3)

  val eq: (Int, Int) => Boolean = _ == _
  val fibs: Stream[Int] =
    0 #:: 1 #:: (fibs zip fibs.tail).map {t => t._1 + t._2}
}
